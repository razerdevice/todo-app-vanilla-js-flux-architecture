class Wrapper {
  constructor(View, mapStateToProps) {
    this.view = new View();
    this.mapStateToProps = mapStateToProps;
  }

  registerComponent(store) {
    this.store = store;
    this.store.subscribe(() => this.render())
  }

  render() {
    const props = this.mapStateToProps(this.store.getState());
    return this.view._render(props, this.store.actions);
  }
}

const connect = (mapStateToProps) => (View) => {
  return new Wrapper(View, mapStateToProps)
}

export default connect;
