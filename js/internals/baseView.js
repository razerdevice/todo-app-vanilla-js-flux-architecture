import {isEqual} from "../utils";

class BaseView {
  constructor(rootId) {
    this.root = document.getElementById(rootId);
    this.prevProps = null;
    this.actions = null;
  }

  afterRender(props) {
    throw new Error('Not implemented!')
  }

  _render(props, actions) {
    if (this.prevProps && isEqual(props, this.prevProps)) {
      return;
    }
    this.prevProps = props;
    this.actions = actions;
    return this.runViewRender(props);
  }

  runViewRender(props) {
    this.root.innerHTML = this.render(props);
    this.afterRender(props);
  }

  render(props) {
    throw new Error('Not implemented!')
  }
}

export default BaseView;
