class Database {
  save(key, data) {
    try {
      localStorage.setItem(key, JSON.stringify(data))
    } catch (e) {

    }
  }

  read(key, fallback) {
    try {
      let data = localStorage.getItem(key);
      try {
        data = JSON.parse(data);
      } catch (e) {
      }
      return data || fallback;
    } catch (e) {
      console.log(e, 'DB error');
      return fallback;
    }
  }
}

export default Database;
