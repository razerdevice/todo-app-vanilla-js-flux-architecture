import {uniqId} from "./utils";
import Database from "./internals/database";
import {ALL} from "./constanats";

class TodoList{
  constructor() {
    this.db = new Database();
    this.subscribers = [];
    this.store = {
      list: this.db.read('items', []),
      filter: this.db.read('filter', ALL),
    }
    this.addItem = this.addItem.bind(this)
    this.removeItem = this.removeItem.bind(this)
    this.toggleCompleted = this.toggleCompleted.bind(this)
    this.completeAll = this.completeAll.bind(this)
    this.clearCompleted = this.clearCompleted.bind(this)
    this.setFilter = this.setFilter.bind(this)
    this.getState = this.getState.bind(this)
    this.subscribe = this.subscribe.bind(this)
    this.actions = {
      addItem: this.addItem,
      removeItem: this.removeItem,
      toggleCompleted: this.toggleCompleted,
      completeAll: this.completeAll,
      clearCompleted: this.clearCompleted,
      setFilter: this.setFilter,
    };
  }


  getState () {
    const { list, filter } = this.store;
    return { list, filter }
  }

  updateStore(newStore) {
    this.store = {
      ...this.store,
      ...newStore,
    };
    this.notifySubscribers();
    this.dbCommit();
  }

  dbCommit() {
    this.db.save('items', this.store.list);
    this.db.save('filter', this.store.filter);
  }

  subscribe(sub) {
    this.subscribers.push({
      id: uniqId(),
      notify: sub,
    })
  }

  notifySubscribers() {
    this.subscribers.forEach(sub => sub.notify());
  }

  setFilter(filter) {
    this.updateStore({ filter });
  }

  addItem({ content }) {
    const list = [...this.store.list, {
      content,
      isCompleted: false,
      id: uniqId(),
    }]
    this.updateStore({ list });
  }

  removeItem({ id }) {
    const list = this.store.list.filter(i => i.id !== id);
    this.updateStore({ list });
  }

  clearCompleted() {
    this.updateStore({ list: this.store.list.filter(i => !i.isCompleted) });
  }

  toggleCompleted({ id }) {
    const list = this.store.list.map(i => {
      if (i.id === id) {
        return { ...i, isCompleted: !i.isCompleted };
      }
      return i;
    });
    this.updateStore({ list });
  }

  completeAll() {
    const isAllCompleted = this.store.list.every(i => i.isCompleted);
    const list = this.store.list.map(i => ({ ...i, isCompleted: !isAllCompleted}));
    this.updateStore({ list });
  }
}

export default new TodoList();
