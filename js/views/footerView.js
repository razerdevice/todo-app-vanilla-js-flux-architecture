import {createFooterLayout} from "../templates";
import {setListener} from "../utils";
import BaseView from "../internals/baseView";
import connect from "../internals/connect";
import {FILTER_OPTIONS} from "../constanats";

class FooterView extends BaseView {
  constructor() {
    super('footer-root');
  }

  afterRender({ list }) {
    setListener('clear-completed', this.actions.clearCompleted);
    FILTER_OPTIONS.forEach(opt => {
      setListener(`opt-${opt}`, () => this.actions.setFilter(opt));
    });
  }

  render({ notCompletedCount, filter }) {
    console.log('FooterView');
    return createFooterLayout(notCompletedCount, filter);
  }
}

const mapStateToProps = ({ list, filter }) => {
  const notCompletedCount = list.filter(i => !i.isCompleted).length;
  return { notCompletedCount, filter };
};

export default connect(mapStateToProps)(FooterView);
