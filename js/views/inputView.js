import {createInputLayout} from "../templates";
import {setListener} from "../utils";
import BaseView from "../internals/baseView";
import connect from "../internals/connect";

class InputView extends BaseView {
  constructor() {
    super('input-container');
  }

  afterRender() {
    setListener('complete-all', () => this.actions.completeAll());
    setListener('input-list', (e) => {
      if (e.key === 'Enter' &&  e.target.value) {
        this.actions.addItem({ content: e.target.value });
        e.target.value = '';
      }
    }, 'keypress');
  }

  render({ isAllCompleted }, actions) {
    console.log('InputView');
    return createInputLayout(isAllCompleted);
  }
}

const mapStateToProps = ({ list }) => {
  const isAllCompleted = list.every(i => i.isCompleted);
  return { isAllCompleted };
}

export default connect(mapStateToProps)(InputView);
