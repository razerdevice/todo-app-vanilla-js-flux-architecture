import {createListLayout} from "../templates";
import {setListener} from "../utils";
import BaseView from "../internals/baseView";
import connect from "../internals/connect";
import {ACTIVE, COMPLETED} from "../constanats";

class ListView extends BaseView {
  constructor() {
    super('list-root');
  }

  afterRender({ list }) {
    list.forEach(({ id }) => {
      setListener(`delete-${id}`, () => this.actions.removeItem({ id }));
      setListener(`checkbox-${id}`, () => this.actions.toggleCompleted({ id }))
    });
  }

  render({ list }) {
    console.log('ListView');
    return createListLayout(list);
  }
}

const mapStateToProps = ({ list, filter }) => {
  const mapped = list.filter(({ isCompleted }) => {
    switch (filter) {
      case ACTIVE:
        return !isCompleted;
      case COMPLETED:
        return isCompleted;
    }
    return true;
  })
  return { list: mapped };
};

export default connect(mapStateToProps)(ListView);
