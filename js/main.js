import model from './model';

import ListView from "./views/listView";
import InputView from "./views/inputView";
import FooterView from "./views/footerView";
import Controller from "./controller";

const app = new Controller(model, [ListView, InputView, FooterView])

app.renderViews();


