class Controller {
  constructor(model, views) {
    this.model = model;
    this.views = views;
    this.views.forEach(view => {
      return view.registerComponent(this.model);
    });
  }

  renderViews() {
    this.views.forEach(view => {
      view.render();
    });
  }
}

export default Controller
