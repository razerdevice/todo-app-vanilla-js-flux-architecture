import {FILTER_OPTIONS} from "./constanats";

export const createListLayout = (list) => {
  return list.map(({ id, content, isCompleted }) => {

    return (
      `
      <div class="list-item" id=${id}>
        <div class="left-item">
          <div class="round" id="checkbox-${id}">
            <input type="checkbox"  ${isCompleted ? 'checked' : ''}/>
            <label for="checkbox" />
          </div>
          <div class="list-item-text  ${isCompleted ? 'text-completed' : ''}">
            ${content}
          </div>
        </div>
        <div class="right-item">
          <div class="close-container">
            <div  class="close-item" id="delete-${id}">
            </div>
          </div>
        </div>
      </div>
    `
    )
  }).join('');
}

export const createInputLayout = (isAllCompleted) => {
  return `
    <div class="complete-all-container">
      <div class="complete-all" id="complete-all">
        <span class="chevron ${isAllCompleted ? 'chevron-active' : ''} bottom" />
      </div>
    </div>
    <input type="text" class="input-list" id="input-list" />
  `;
}

export const createFooterLayout = (notCompletedCount, filter) => {
  const activeClass = (type) => type === filter ? 'footer-button-active' : '';
  return `
    <div class="footer-left">
      <div class="footer-counter">${notCompletedCount} items left</div>
    </div>
    <div class="footer-center-row">
      ${FILTER_OPTIONS.map(opt => (
        `
          <div class="footer-button ${activeClass(opt)}" id="opt-${opt}">
            ${opt}
          </div>
        `
       )).join('')}
    </div>
    <div class="footer-right">
      <div class="footer-button" id="clear-completed">Clear complete</div>
    </div>
  `;
}
